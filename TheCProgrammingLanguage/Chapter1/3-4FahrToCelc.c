#include <stdio.h>

int main() {
  float celc, fahr;
  int lowerLimit = 0, upperLimit = 300, increments = 10;
  fahr = lowerLimit; 

  printf("%s\t%s\n", "Celcius", "Fahrenheit");
  while (fahr <= upperLimit) {
    celc = (5.0/9.0) * (fahr-32.0);
    printf("%7.2f\t %9.0f\n", celc, fahr);
    fahr += increments;
  }
  return 0;
}
