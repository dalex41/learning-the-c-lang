#include <stdio.h>
#define ARRSIZE 2048

int getLine(char lineIn[]);
void reverseLine(char lineIn[], char lineReversed[], int lineLen);

int main() {
  char lineIn[ARRSIZE];
  char lineReversed[ARRSIZE];
  int lineLen;

  while ((lineLen = getLine(lineIn)) > 0) {
    reverseLine(lineIn, lineReversed, lineLen-2);
    printf("%s", lineReversed);
  }
  return 0;
}

int getLine(char lineIn[]) {
  char inC;
  int i;
  for (i = 0; i < ARRSIZE && (inC = getchar()) != EOF && inC != '\n'; i++) {
    lineIn[i] = inC;
  }

  if (inC == '\n')
    lineIn[i++] = inC;

  lineIn[i] = '\0';
  return i;
}

void reverseLine(char lineIn[], char lineReversed[], int lineLen) {
  int i = 0;
  while (lineLen >= 0) {
    lineReversed[i++] = lineIn[lineLen--];
  }
  lineReversed[i++] = '\n';
  lineReversed[i] = '\0';
}
