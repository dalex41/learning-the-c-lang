#include <stdio.h>

int main() {
  printf("%s%c\n", "Char value of EOF: ", EOF);
  printf("%s%d\n", "Int value of EOF: ", EOF);
  printf("%s%ld\n", "Long value of EOF: ", EOF);
  printf("%s%f\n", "Float value of EOF: ", EOF);
  int c;
  while (c = getchar() != EOF) {
    printf("%d\n", c);
  }
  return 0;
}
