#include <stdio.h>
#define OUTWORD 0
#define INWORD 1
#define ARRLEN 100
#define HISTVERTICAL 1

void printHistogram(int *wordLen) {
  if (HISTVERTICAL == 0) {
    for (int i = 0; i < ARRLEN; i++) {
      if (wordLen[i] > 0) {
      printf("%2d%s", i, ": ");
      for (int j = wordLen[i]; j > 0; j--)
        putchar('-');
      putchar('\n');
      }
      }
  } else {
    int barCount = 0;
    int maxHeight = 0;
    for (int i = 0; i < ARRLEN; i++) {
      if (wordLen[i] > 0) {
        barCount++;
        if (wordLen[i] > maxHeight)
          maxHeight = wordLen[i];
      }
    }
    int histWordLen[barCount];
    int histWordCount[barCount];
    int histIndex = 0;
    for (int i = 0; i < ARRLEN; i++) {
      if (wordLen[i] > 0) {
        histWordLen[histIndex] = i;
        histWordCount[histIndex++] = wordLen[i];
      } 
    }

    for (int i = 1; i <= maxHeight; i++) {
      for (int j = 0; j < barCount; j++) {
        putchar(' ');
        putchar(' ');
        if (maxHeight-histWordCount[j]-i >= 0)
          putchar(' ');
        else
         putchar('|');
      }
      putchar('\n');
    }
    for (int i = 0; i < barCount; i++) {
      printf("%3d", histWordLen[i]);
    }
    putchar('\n');
  }
}
int main() {
  char inC;
  int wordState = INWORD;
  int wordLen[ARRLEN];
  int currWordL = 0;

  for (int i = 0; i < ARRLEN; i++) {
    wordLen[i] = 0; 
  }
  while ((inC = getchar()) != EOF) {
    //Print histogram on 
    if (inC == '\n') {
      if (currWordL > 0) {
        wordLen[currWordL]++;
        currWordL = 0;
      }
      printHistogram(wordLen);
      for (int i = 0; i < ARRLEN; i++) {
        wordLen[i] = 0; 
      }
    } else if (inC == ' ' || inC == '\t') {
      wordState = OUTWORD;  
    } else {
      if (wordState == OUTWORD && currWordL > 0) {
        wordLen[currWordL]++;
        currWordL = 0;
      }
      wordState = INWORD;
      currWordL++;
    }
  }

  return 0;
}
