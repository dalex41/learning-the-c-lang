#include <stdio.h>

int main() {
  int c;
  short b = 0, t = 0, nL = 0;

  while ((c = getchar()) != EOF)
    switch (c) {
      case ' ':
        b++;
        break;
      case '\t':
        t++;
        break;
      case '\n':
        nL++;
        if (b != 0)
          printf("%s%d\n", "Space count: ", b);
        if (t != 0)
          printf("%s%d\n", "Tab count: ", t);
        printf("%s%d\n", "NewLine count: ", nL);
        break;
    }
  

  return 0;
}
