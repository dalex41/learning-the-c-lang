#include <stdio.h>
#define INWORD 1
#define OUTWORD 0

int main() {
  char inC;
  int wordState = INWORD;

  
  while ((inC = getchar()) != EOF) {
    if (inC == ' ' || inC == '\t' || inC == '\n') {
      wordState = OUTWORD;
    } else { 
      if (wordState == OUTWORD)
        putchar('\n');
      wordState = INWORD;
      putchar(inC);
    }
  }
  return 0;
}
