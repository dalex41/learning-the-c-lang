#include <stdio.h>
#define MAXLINE 1024

int getLine(char lineIn[]);
void removeTrailingWhitespace(char newLine[], int lineLen);

int main() {
  int lineLen;
  char lineIn[MAXLINE];
  
  while ((lineLen = getLine(lineIn)) > 0) {
    removeTrailingWhitespace(lineIn, lineLen-2);
    printf("%s", lineIn);
  }
  return 0;
}

int getLine(char lineIn[]) {
  char inC;
  int len;

  for (len = 0; len < MAXLINE && (inC = getchar()) != EOF && inC != '\n'; len++) {
    lineIn[len] = inC;
  }
  if (inC == '\n')
    lineIn[len++] = inC;
  lineIn[len] = '\0';
  return len;
}

void removeTrailingWhitespace(char newLine[], int lineLen) {
  while (newLine[lineLen] == '\t' || newLine[lineLen] == ' ') {
    lineLen--;
  }
  if (lineLen == -1) {
    newLine[0] = '\0';
  } else {
    newLine[++lineLen] = '\n';
    newLine[++lineLen] = '\0';
  }
}
