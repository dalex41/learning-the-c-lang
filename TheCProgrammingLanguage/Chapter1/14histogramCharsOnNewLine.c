#include <stdio.h>
#define ARRLEN 100
#define HISTVERTICAL 1

void printHistogram(char *charsGiven, int *charsCount) {
  if (HISTVERTICAL == 0) {
    for (int i = 0; i < ARRLEN; i++) {
      if (charsCount[i] > 0) {
       printf("%c%s", charsGiven[i], ": ");
       for (int j = 0; j < charsCount[i]; j++)
        putchar('-'); 
      putchar('\n');
      }
    }
  } else {
    int barCount = 0;
    int maxHeight = 0;
    for (int i = 0; i < ARRLEN; i++) {
      if (charsCount[i] > 0) {
        barCount++;
        if (charsCount[i] > maxHeight)
          maxHeight = charsCount[i];
      }
    }

    for (int i = 1; i <= maxHeight; i++) {
      for (int j = 0; j < barCount; j++) {
        putchar(' ');
        if (maxHeight-charsCount[j]-i >= 0)
          putchar(' ');
        else
         putchar('|');
      }
      putchar('\n');
    }
    for (int i = 0; i < barCount; i++) {
      printf("%2c", charsGiven[i]);
    }
    putchar('\n');
  }
}

int main() {
  char inC;
  char charsGiven[ARRLEN];
  int charsCount[ARRLEN];
  int currentCharIndex = 0;

  for (int i = 0; i < ARRLEN; i++) {
    charsCount[i] = 0; 
  }
  while ((inC = getchar()) != EOF) {
    if (inC != '\n') {
    for (int i = 0; i <= currentCharIndex; i++) {
      if (inC == charsGiven[i]) {
        charsCount[i]++;
        break;
      } else if (i == currentCharIndex) {
        charsGiven[i] = inC;
        charsCount[i]++;
        currentCharIndex++;
        break;
      }
      }
    }else {
      printHistogram(charsGiven, charsCount);
      for (int i = 0; i < ARRLEN; i++)
        charsCount[i] = 0; 
      currentCharIndex = 0;
    }
  }
  return 0;
}

