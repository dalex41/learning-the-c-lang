#include <stdio.h>

int main() {
  char currC;
  while ((currC = getchar()) != EOF)
    switch (currC) {
      case '\t':
        putchar('\\');
        putchar('t');
        break;
      case '\b':
        putchar('\\');
        putchar('b');
        break;
      case '\\':
        putchar('\\');
        putchar('\\');
        break;
      default:
        putchar(currC);
    }
  
  return 0;
}
