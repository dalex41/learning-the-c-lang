#include <stdio.h>
void printEscapeCharacter(char* type, char escChar);

int main() {
  printEscapeCharacter("Newline", '\n');
  printEscapeCharacter("Horizontal tab", '\t');
  printEscapeCharacter("Alert", '\a');
  printEscapeCharacter("Backspace", '\b');
  printEscapeCharacter("Carriage return", '\r');
  printEscapeCharacter("Backslash", '\\');
  printEscapeCharacter("Quotes", '\"');
  printEscapeCharacter("Question mark", '\?');
  return 0;
}

void printEscapeCharacter(char* type, char escChar) {
  printf("%s", type);
  printf(" ");
  printf("%c", escChar);
  printf(" ");
  printf("%s", "some text without prior newline");
  printf("\n");
}
