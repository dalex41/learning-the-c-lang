#include <stdio.h>
#define STARTVAL 0
#define ENDVAL 500
#define TEMPINCREMENTS 20

float calcCelc(int farhVal);

int main() {
  printf("%s\t%s\n", "Fahrenheit", "Celcius");
  for (int i = STARTVAL; i <= ENDVAL; i += TEMPINCREMENTS) {
    printf("%10d\t%7.2f\n", i, calcCelc(i));
  } 

  return 0;
}

float calcCelc(int fahrVal) {
  return (5.0/9.0)*(fahrVal-32);
}
