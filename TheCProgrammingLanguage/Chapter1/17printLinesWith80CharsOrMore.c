#include <stdio.h>
#define MINCHARSPERLINE 80
#define ARRSIZE 2048

int getLine(char lineIn[]);
int main() {
  char lineIn[ARRSIZE];
  int lineLen;

  while ((lineLen = getLine(lineIn)) > 0) {
    if (lineLen > MINCHARSPERLINE)
      printf("%s", lineIn);
  }
  return 0;
}

int getLine(char lineIn[]) {
  char inC;
  int i;
  for (i = 0; i < ARRSIZE && (inC = getchar()) != EOF && inC != '\n'; i++) {
    lineIn[i] = inC;
  }

  if (inC == '\n')
    lineIn[i++] = inC;

  lineIn[i] = '\0';
  return i;
}
