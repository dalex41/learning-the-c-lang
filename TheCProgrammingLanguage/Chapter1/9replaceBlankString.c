#include <stdio.h>

int main() {
  char prevC = -1, currC;

  while ((currC = getchar()) != EOF) {
    if (prevC == ' ') {
      if (currC != ' ') {
        putchar(' ');
      }
    } else {
      putchar(prevC);
    }
    
    prevC = currC;
  }
  return 0;
}
