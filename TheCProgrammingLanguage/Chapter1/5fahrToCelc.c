#include <stdio.h>
#define LOWERLIMIT 0
#define UPPERLIMIT 300
#define INCREMENTS 10

int main() {

  printf("%s\t%s\n", "Fahreinheit", "Celcius");
  for (int fahr = UPPERLIMIT; fahr >= LOWERLIMIT; fahr -= INCREMENTS) {
    printf("%11d\t %6.2f\n", fahr, (5.0/9.0) * (fahr-32.0));
  }
  return 0;
}
