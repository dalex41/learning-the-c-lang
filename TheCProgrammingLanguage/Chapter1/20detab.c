#include <stdio.h>
#define BLANKSTOP 5
#define ARRSIZE 1024
int getLine();
char lineIn[ARRSIZE];

int main() {
    while (getLine() > 0) {
      printf("%s", lineIn);
    }

}

int getLine() {
  char inC;
  int i;
  char currWord[ARRSIZE];
  for (i = 0; i < ARRSIZE && (inC = getchar()) != EOF && inC != '\n'; i++) {
          lineIn[i] = inC;
  }

  if (inC == '\n')
    lineIn[i++] = inC;

  lineIn[i] = '\0';
  return i;
}
