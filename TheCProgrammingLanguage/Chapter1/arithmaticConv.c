#include <stdio.h>
int main() {
  int d1 = 10;
  float f1 = 20.17;
  printf("%s\t%d\t%.2f\n", "Two values: ", d1, f1);

  printf("%s\t%d\t%.2f\n", "Printing sum in format of int then float: ", d1+f1, d1+f1);
  return 0;
}
