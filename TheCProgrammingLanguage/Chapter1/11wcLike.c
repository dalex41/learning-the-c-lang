#include <stdio.h>
#define INWORD 1
#define OUTWORD 0

int main() {
  char inC;
  int cNl, cW, cB, wordState;
  cNl = cW = cB = 0;
  wordState = OUTWORD;

  
  while ((inC = getchar()) != EOF) {
    if (inC == ' ' || inC == '\t' || inC == '\n') {
      if (inC == '\n')
        cNl++;
      wordState = OUTWORD;
    } else if (wordState == OUTWORD) {
      wordState = INWORD;
      cW++;
    }
    cB++;
  }
  printf("%s\t%d\t%d\t%d\n", "new line count, word count, byte count",
         cNl, cW, cB);
  return 0;
}
