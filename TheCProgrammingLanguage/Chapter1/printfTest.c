#include <stdio.h>
int main() {
  printf("%s\n", "%s = string; %d = int; %f = float; %2d = dec. int, at least 2 chars;");
  printf("%s\n", "%.3f = float, 2 chars after decimal point; %5.5f = float at least 5 wide and 5 after decimal point;");
  printf("%s\n", "%o = octal, %x = hex; %c = char; %% = percent sign");
  printf("%%\n");
  float f1 = 22.61;
  int i1 = 5583;
  const char* myString = "String in the form of char array";
  printf("%s\t%s\n", "tab after this string", "new line after this one");
  printf("%s\t%5f\n", "some float value (right-justified): ", f1);
  printf("%s\t%5.1f\n", "some float value (right-justified) with one decimal point: ", f1);
  printf("%s\t%5d\n", "some int value: ", i1);
  printf("%s\t%10s\n", "string from char arr: ", myString);
  return 0;
}
