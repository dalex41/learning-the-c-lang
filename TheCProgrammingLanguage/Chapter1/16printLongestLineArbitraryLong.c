#include <stdio.h>
#define STARTARRLEN 256

int getLine(char currLineIn[], int currLen);
void copy(char to[], char from[]);

int main() {
  int currLineLen;
  int maxLineLen = 0;
  int maxArrSize = STARTARRLEN;
  char currLineIn[STARTARRLEN];
  char maxLineIn[STARTARRLEN];

  while ((currLineLen = getLine(currLineIn, 0)) > 0) {

    if (currLineLen > maxLineLen) {
      maxLineLen = currLineLen;
      copy(maxLineIn, currLineIn);
    }
  }
  printf("%s%d%s\n", "Longest line given with ", maxLineLen, " chars:");
  printf("%s", maxLineIn);
}

int getLine(char currLineIn[], int currLen) {
  int lineLen = currLen;
  char inC;
  for (; (inC = getchar()) != EOF && inC != '\n'; lineLen++) {
    if (lineLen == sizeof(*currLineIn)) {
      char extLineIn[sizeof(*currLineIn)*2];
      copy(extLineIn, currLineIn);
      return lineLen + getLine(extLineIn, lineLen);
    }
    currLineIn[lineLen] = inC;
  }
  if (inC == '\n')
    currLineIn[lineLen++] = inC;
  currLineIn[lineLen] = '\0';
  printf("%s\t%s\n", "test: ", currLineIn);
  return lineLen;
}

void copy(char to[], char from[]) {
  int i = 0;
  while (from[i] != '\0') {
    to[i] = from[i];
    i++;
  }
  to[i] = '\0';
}
