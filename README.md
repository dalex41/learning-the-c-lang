# Learning low-er level stuff
This repo contains exercices I do along my journey with learning low level CS stuff.  
Here are some goals goals I want to achieve in the near future:  
    [ ] Get used to writing more complex programs in C  
    [ ] Finally learn X86 assembly  
    [ ] Improve on my knowledge of DSA and explore more advanced topics  
    [ ] NandToTetris?  

## The C programming language 
26.01.2024  
My first resource will be this famous piece, by Brian Kernighan and Dennis Ritchie.  
