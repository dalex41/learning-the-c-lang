#include <stdio.h>

int main() {
  int x = 722;
  printf("%s", "Value of x: ");
  printf("%d", x);
  printf("%s", "\n");
  int *ptrX = &x;
  printf("%s", "Address of x: ");
  printf("%p", ptrX);
  printf("%s", "\n");
  int **ptrPtrX = &ptrX;
  printf("%s", "Address of ptr of x: ");
  printf("%p", ptrPtrX);
  printf("%c", '\n');
  **ptrPtrX = 51;
  printf("%s", "Value of x after double pointer dereference and assignement: ");
  printf("%d", x);
  printf("%c", '\n');
  return 0;
}
