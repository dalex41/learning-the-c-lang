#include <stdio.h>
#include <stdbool.h>



int main(int argc, char *argv[]) {
  printf("Arg count: ");
  printf("%d", argc-1);
  printf("\n");
  if (argc-1 != 3) {
    printf("Not enugh arguments! Aborting...\n");
    return 1;
  }
  const int userInVal1 = (int)(*argv[1])-'0';
  const int userInVal2 = (int)(*argv[2])-'0';
  int result; 
  switch (*argv[3]) {
    case '+':
      result = userInVal1+userInVal2; 
      break;
    case '-':
      result = userInVal1-userInVal2;
      break;
    case '*':
      result = userInVal1*userInVal2;
      break;
    case '/':
      if (userInVal2 == 0) {
        printf("Can't divide by 0!\n");
        return 1;
      }
      result = userInVal1/userInVal2;
      break;
    default:
      printf("%s", "No operation found!\n");
      return 1;
    } 
  printf("Result: ");
  printf("%d", result);
  printf("\n");
  return 0; }
